CREATE DATABASE db_web;

-- USE db_web;

CREATE TABLE people (

   ID INT(11) NOT NULL AUTO_INCREMENT,
   FNAME VARCHAR(255) NOT NULL,
   LNAME VARCHAR(255),
   EMAIL VARCHAR (255),
   PRIMARY KEY (ID)

) AUTO_INCREMENT=1;

CREATE TABLE feedback (

   ID INT(11) NOT NULL AUTO_INCREMENT,
   TITLE VARCHAR (20) NOT NULL,
   CONTENT VARCHAR(255) NOT NULL,
   PID INT(11) NOT NULL,
   PRIMARY KEY (ID),
   FOREIGN KEY (PID) REFERENCES tbl_people(ID)

) AUTO_INCREMENT=1;

CREATE TABLE registration(

    ID int(10) NOT NULL AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    email varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    PRIMARY KEY (id)

)

-- *********CREATE PEOPLE*********

INSERT INTO people (FNAME,LNAME,EMAIL) VALUES ('Jimmy', 'Hendrix', 'sample321@hotmail.co.nz');                                                 
INSERT INTO people (FNAME,LNAME,EMAIL) VALUES ('Eric', 'Clapton', 'example1658@hotmail.co.nz');
INSERT INTO people (FNAME,LNAME,EMAIL) VALUES ('Ozzy', 'Osbourne', 'new389@hotmail.co.nz');                                                  
INSERT INTO people (FNAME,LNAME,EMAIL) VALUES ('Brian', 'Johnson', 'example97@gmail.com');
INSERT INTO people (FNAME,LNAME,EMAIL) VALUES ('Freddy', 'Mercury', 'sample321@gmail.com');
INSERT INTO people (FNAME,LNAME,EMAIL) VALUES ('Mick', 'Jagger', 'new579@gmail.com');

-- SELECT * FROM people;
-- UPDATE people SET FNAME = 'Jeff' WHERE ID = 1;
-- DELETE FROM people WHERE ID = 1;


-- *********CREATE POSTS*********

INSERT INTO feedback (TITLE, CONTENT, PID) VALUES ('LOREM IPSUM', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nobis, doloremque.', 6);
INSERT INTO feedback (TITLE, CONTENT, PID) VALUES ('LOREM IPSUM', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi repellat vitae magni et exercitationem non optio dolorem qui iure similique!', 2);
INSERT INTO feedback (TITLE, CONTENT, PID) VALUES ('LOREM IPSUM', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae, illo.', 1);
INSERT INTO feedback (TITLE, CONTENT, PID) VALUES ('LOREM IPSUM', 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Excepturi, modi?', 4);
INSERT INTO feedback (TITLE, CONTENT, PID) VALUES ('LOREM IPSUM', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nobis, doloremque.', 2);
INSERT INTO feedback (TITLE, CONTENT, PID) VALUES ('LOREM IPSUM', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi repellat vitae magni et exercitationem non optio dolorem qui iure similique!', 3);

-- SELECT * FROM feedback;
-- UPDATE feedback SET TITLE = 'TITLE1' WHERE ID = 1;
-- DELETE FROM feedback WHERE ID = 5;

-- DROP TABLE feedback;

-- *********CREATE RESGISTERS*********


INSERT INTO registration (name,email,password) VALUES ('Jimmy Hendrix', 'sample321@hotmail.co.nz', 'password1');                                                 
INSERT INTO registration (name,email,password) VALUES ('Eric Clapton', 'example1658@hotmail.co.nz', 'password2');
INSERT INTO registration (name,email,password) VALUES ('Ozzy Osbourne', 'new389@hotmail.co.nz', 'password3');                                                  
INSERT INTO registration (name,email,password) VALUES ('Brian Johnson', 'example97@gmail.com', 'password4');
INSERT INTO registration (name,email,password) VALUES ('Freddy Mercury', 'sample321@gmail.com', 'password5');
INSERT INTO registration (name,email,password) VALUES ('Mick Jagger', 'new579@gmail.com', 'password6');

