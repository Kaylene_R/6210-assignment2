<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

   
    <title>Home Page</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap.min.css" rel="stylesheet">
    <link href="public\css\bootstrap\bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="index.php"> <img src="~/public/image/logo2.png" alt="logo"> Ethereal Beauty</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="contact.php">Contact</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="signup.php">Sign Up</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <header>
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <!-- Slide One - Set the background image for this slide in the line below -->
          <div class="carousel-item active" style="background-image: url('image/Banner .jpg')">
            <div class="carousel-caption d-none d-md-block">
              
            </div>
          </div>
          <!-- Slide Two - Set the background image for this slide in the line below -->
          <div class="carousel-item" style="background-image: url('image/slide2.jpg')">
            <div class="carousel-caption d-none d-md-block">
              
            </div>
          </div>
          <!-- Slide Three - Set the background image for this slide in the line below -->
          <div class="carousel-item" style="background-image: url('image/slide3.jpg')">
            <div class="carousel-caption d-none d-md-block">
              
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </header>

    <!-- Page Content -->
    <div class="container">

      <h1 class="my-4">Welcome to Ethereal Beauty</h1>
      <!-- About Section -->
      <div class="row">
        <div class="col-lg-6">
          <h2>About</h2>
          <p> This is a beauty website. Its main purpose is to provide an array of beauty services, but this renownded website is also used as a platform for all makeup and beauty lovers. On this platform customers from all over the world may use this website as a forum to review beauty products of all kinds as well as share their own personal experiences about using said products, thus customers will be able to relate or debate upon the use of any products available on the market. </p>
          <p>Services of Ethereal Beauty include:</p>
          <ul>
            <li>Custom Makeovers</li>
            <li>A Consultation</li>
            <li>Beauty Lessons</li>
          </ul>
        </div>
        <div class="col-lg-6">
        <a class="twitter-timeline"  href="https://twitter.com/hashtag/makeuplover" data-widget-id="980385851573071872">#makeuplover Tweets</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
          
        </div>
      </div>

      <!-- Services Section -->
      <div class="row">
        <div class="col-lg-4 mb-4">
          <div class="card h-100">
            <h4 class="card-header">Custom Makeovers</h4>
            <div class="card-body">
              <p class="card-text">We can provide a variety of custom makeovers for any ocassion, using a variety of different techiques, products and styles. We provide makeovers for weddings, a school ball, and we even provide FX for halloween or a convention of some sort.</p>
            </div>
            <div class="card-footer">
              <a href="contact.php" class="btn btn-primary">Make an appointment</a>
            </div>
          </div>
        </div>
        <div class="col-lg-4 mb-4">
          <div class="card h-100">
            <h4 class="card-header">Consultations</h4>
            <div class="card-body">
              <p class="card-text"> Our own beauticians give customers a consultation service to give you advice on anything concerning hair of beauty. Our team can do anything from helping you decide which products work for you, to giving advice on what look would work for a specific occasion i.e. dates, weddings or even for just going to a party with your friends.  </p>
            </div>
            <div class="card-footer">
              <a href="contact.php" class="btn btn-primary">Make an appointment</a>
            </div>
          </div>
        </div>
        <div class="col-lg-4 mb-4">
          <div class="card h-100">
            <h4 class="card-header">Lessons</h4>
            <div class="card-body">
              <p class="card-text">We have a large range beauty classes. An instructor woll help you learn everything from skincare basic to the latest makeup trends. Our differnt classes include Fenty Beauty Foundation Class, Age-Defying Skincare, Tenn Makeup and more. Simply contact us, give a brief description of what you wish to learn, and we'll suggest the lesson(s) that suit your needs </p>
            </div>
            <div class="card-footer">
              <a href="contact.php" class="btn btn-primary">Make an appointment</a>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->

      <!-- Portfolio Section -->
      <h2>Testimonials</h2>

      <div class="row">
        <div class="col-lg-4 col-sm-6 testimonial-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="image/testimonial1.png" alt="t1"></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Testimonial One</a>
              </h4>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur eum quasi sapiente nesciunt? Voluptatibus sit, repellat sequi itaque deserunt, dolores in, nesciunt, illum tempora ex quae? Nihil, dolorem!</p>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 testimonial-item">
          <div class="card h-100">
            <img class="card-img-top" src="image/testimonial2.png" alt="t2"></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Testimonial Two</a>
              </h4>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 testimonial-item">
          <div class="card h-100">
            <a href="#"><img class="card-img-top" src="image/testimonial3.png" alt="t3"></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Testimonial Three</a>
              </h4>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos quisquam, error quod sed cumque, odio distinctio velit nostrum temporibus necessitatibus et facere atque iure perspiciatis mollitia recusandae vero vel quam!</p>
            </div>
          </div>
        </div>
      </div>
      <br>
      <br>
      <h2>Feedback</h2>
      <div class="row">
        
        
        <div class="col-lg-4 col-sm-6 testimonial-item">
          <div class="card h-100">
          <a href="#"><img class="card-img-top" src="image/feedback1.png" alt="f1"></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Feedback 1</a>
              </h4>
              <p class="card-text">The best decision I ever made for myself ! EB is a great place to learn everything you need to know about makeup! And u can retake the classes if you want at no additional charge ! You’ll love the school if you really wanna learn ! <br> 
                                    <i> - Jamie Smith </i>  </p>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 testimonial-item">
          <div class="card h-100">
          <a href="#"><img class="card-img-top" src="image/feedback2.png" alt="f2"></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Feedback 2</a>
              </h4>
              <p class="card-text">If you haven’t attended a beauty class at Ethereal Beauty, put it on your to-do list! I like to think I’m pretty savvy in terms of makeup and beauty trends, but there’s always something new to learn! <br>
                                   <i> - Sarah Greene </i> </p>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 testimonial-item">
          <div class="card h-100">
          <a href="#"><img class="card-img-top" src="image/feedback3.png" alt="f3"></a>
            <div class="card-body">
              <h4 class="card-title">
                <a href="#">Feedback 3</a>
              </h4>
              <p class="card-text"> The makeup artist gave me a makeover for comic con. I went as Kratos from God of War. I spent hours putting on makeup, but it was all worth it. The amount of detail put into the makeup was AMAZING!Seeing myself as Kratos just made it easier to embody the character.<br>
                                  <i> - Steven Erikson </i> </p>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->

      
      <!-- /.row -->

      <hr>

      

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Ethereal Beauty 2018</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="jquery.min.js"></script>
    <script src="bootstrap.bundle.min.js"></script>

  </body>

</html>
