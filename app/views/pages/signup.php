<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sign Up Page</title>

    <!-- Bootstrap core CSS -->
    <link href="~/css/bootstrap/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="public/css/wrapperstyle.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script type="text/javascript" src="public/scripts/form-validation"> </script>



  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="index.php"><img src="image/logo2.png" alt="logo">Ethereal Beauty</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="contact.php">Contact</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="signup.php">Sign Up</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>


    <header>
      
    </header>

    <!-- Page Content -->
    <div id="container">
          <div id="wrapper">
                    <br>
                    <br>
                    <br>
              <div class="form_div">
                    <p class="form_label">SIGNUP FORM</p>
                    <br>
                    <form action="policydocs.php" method="post">
                    <a href="policydocs.php">Download Policy Document</a>
                    <br>
                    </form>
                    <br>
                    <br>
                    <form class="form" method="post" action="#">
                    <input id="name" type="text" name="name" placeholder="Enter Full Name"> 	
                    <input id="email" type="text" name="email" placeholder="your@email.com">
                    <input id="password" type="password" name="password" placeholder="**********"></p>
                    <input type="button" name="register" id="register" value="SIGNUP">
                    </form>
                    <br>
                    <br>
              </div>
          
          </div>
    </div>
    <!-- /.container -->
    
    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Ethereal Beauty 2018</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="~/scripts/js/jquery.min.js"></script>
    <script src="~/scripts/js/bootstrap.bundle.min.js"></script>
    

    

    
  </body>

</html>
